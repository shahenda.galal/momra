$(document).ready(function(){
    $('.services').owlCarousel({
      rtl:true,
      margin:10,
      nav:false,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:2
            },
            1025:{
                items:3
            }
        }
    });
    $('.news').owlCarousel({
        nav:true,
        rtl:true,
		dots:false,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            1025:{
                items:2
            }
        }
    });
    $('.activities').owlCarousel({
      margin:10,
      nav:false,
      rtl:true,
      responsive:{
          0:{
              items:1
          },
          768:{
              items:2
          },
          1025:{
              items:3
          }
      }
  });
    $('.events').owlCarousel({
        margin:10,
      nav:false,
      rtl:true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:2
            },
            1025:{
                items:3
            }
        }
    });
    // share button collapse
    if($('.share-btn').length  > 0){
        $('.share-icons').hide();
        $('.share-btn').click(function(){
            $(this).toggleClass('active');
            $('.share-icons').toggle();
        });
    } 
});















































$('#myCarousel').carousel({
interval: 4000
})

$('.carousel .item').each(function(){
var next = $(this).next();
if (!next.length) {
next = $(this).siblings(':first');
}
next.children(':first-child').clone().appendTo($(this));

for (var i=0;i<2;i++) {
next=next.next();
if (!next.length) {
next = $(this).siblings(':first');
}

next.children(':first-child').clone().appendTo($(this));
}
});



$(document).ready(function() {
    $('#fullpage').fullpage({
      navigation: true,


		responsiveWidth: 768,
		responsiveSlides: false,


      navigationPosition: 'right',
      navigationTooltips: [
        'مستجدات',
        'الكلمة',
        'خدمات',
        'لوائح',
        'أخبار',
        'بيانات',
        'مبادرات',
        'فعاليات',
      ],
      showActiveTooltip: true,
      slidesNavigation: true,
        slidesNavPosition: 'bottom',
      controlArrows:false,
    });
});





/*Bootstrap Carousel Touch Slider.
 http://bootstrapthemes.co

 Credits: Bootstrap, jQuery, TouchSwipe, Animate.css, FontAwesome*/

// Touch Slider

$(document).ready(function(){
  $('#bootstrap-touch-slider').bsTouchSlider();
});


( function ( $ ) {
    "use strict";

    $.fn.bsTouchSlider = function ( options ) {
        var carousel = $( ".carousel" );
        return this.each( function ( ) {

            function doAnimations( elems ) {
                //Cache the animationend event in a variable
                var animEndEv = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                elems.each( function ( ) {
                    var $this = $( this ),
                        $animationType = $this.data( 'animation' );
                    $this.addClass( $animationType ).one( animEndEv, function ( ) {
                        $this.removeClass( $animationType );
                    } );
                } );
            }

            //Variables on page load
            var $firstAnimatingElems = carousel.find( '.item:first' ).find( "[data-animation ^= 'animated']" );
            //Initialize carousel
            carousel.carousel( );
            //Animate captions in first slide on page load
            doAnimations( $firstAnimatingElems );
            //Other slides to be animated on carousel slide event
            carousel.on( 'slide.bs.carousel', function ( e ) {
                var $animatingElems = $( e.relatedTarget ).find( "[data-animation ^= 'animated']" );
                doAnimations( $animatingElems );
            } );
            //swipe initial
            $( ".carousel .carousel-inner" ).swipe( {
                swipeLeft: function ( event, direction, distance, duration, fingerCount ) {
                    this.parent( ).carousel( 'next' );
                },
                swipeRight: function ( ) {
                    this.parent( ).carousel( 'prev' );
                },
                threshold: 0
            } );

        } );
    };


} )( jQuery );
